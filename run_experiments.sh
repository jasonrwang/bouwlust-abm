#!/bin/sh

# Run this file on macOS (with NetLogo installed for all users) to run the experiment

/Applications/NetLogo\ 6.1.1/netlogo-headless.sh \
  --model final_practical/final_practical.nlogo \
  --setup-file final_practical/experiments.xml \
  --experiment experiment \
  --table analysis/data/final_practical\ experiment-table.csv \
  --threads 8
