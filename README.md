[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jasonrwang%2Fbouwlust-abm/master?urlpath=lab/tree/analysis/analysis.md)

# bouwlust-abm

An agent-based model of the Bouwlust neighbourhood for group 9's SEN1211 final project.

Built by:

- Fatima Debbaghi (4780388)
- Ruchik Patel (4787226)
- Jason R Wang (4788281)

## Introduction

An agent-based model of Bouwlust, its citizens, police, and community workers, and other properties of the community. The model is intended to offer insight into how policy interventions from the Municipality of The Hague can improve the quality of life in Bouwlust for all its citizens.

The model is implemneted in NetLogo (see the file `final_practical/final_practical.nlogo`)

All the analysis is contained in the Jupyter Notebook `analysis/analysis.md` (use Jupytext or the Binder link above to run the file and see the outputs).

## Download

If you are only interested in running the model file, you can [download it directly](https://gitlab.com/jasonrwang/bouwlust-abm/-/raw/master/final_practical/final_practical.nlogo?inline=false) or download [this repository as a ZIP](https://gitlab.com/jasonrwang/bouwlust-abm/-/archive/master/bouwlust-abm-master.zip).

If you want to review our analysis, you can use our Binder environment linked above (it may take some time to load) or do it on your own computer. Note that due to the large simulation output files, you will need to install git-lfs before you clone the repository.
