import random
import numpy as np
import math

numPeople = 2770
# yearlyProb = 0.25
# dailyProbability = (1 - (1 - yearlyProb) ** (1/365))
dailyProbability = 4/365
replications = 1000

def perPerson():
    eventsPerPerson = [random.random() <= dailyProbability for days in range(365)]
    return np.array(eventsPerPerson).sum()

def perReplication(replications):
    eventsPerPerson = [perPerson() for repls in range(replications)]
    return np.array(eventsPerPerson).mean()

overallOccurences = [perReplication(replications) for people in range(numPeople)]
# overallOccurences = [perPerson() for people in range(numPeople)]
print("{:.5f}".format(np.array(overallOccurences).sum()/numPeople))