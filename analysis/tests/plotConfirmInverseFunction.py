import matplotlib.pyplot as plt
import numpy as np
from math import (exp, log)

initial_number_people = 2770
pls_upper_threshold = 500
burglaryupper = (0.10 / 365 * initial_number_people / 27505)
burglarylower = (0.000 / 365 * initial_number_people / 27505)

# Inverse y = a/(x+c)
c_inv = (burglarylower * pls_upper_threshold / (burglaryupper - burglarylower))
a_inv = (burglarylower * (pls_upper_threshold + c_inv))
y_inv = []

# Parabolic y = a(x+c)^2
c_para = pls_upper_threshold/((burglarylower/burglaryupper) ** (1/2) -1)
a_para = burglarylower/(pls_upper_threshold+c_para) ** 2
y_para = []

# Exponential y = 1 / e^(ax+c)
a_exp = log(burglaryupper/burglarylower)/pls_upper_threshold
c_exp = log(1 / (exp(a_exp * pls_upper_threshold) * burglarylower))
y_exp = []

# Linear y = ax+c
c_lin = burglaryupper
a_lin = (burglarylower - c_lin)/pls_upper_threshold
y_lin = []

plsglobal = [i for i in range(0,int(pls_upper_threshold * 1.2))]

for i in plsglobal:
    if i < pls_upper_threshold:
        y_inv.append(a_inv / (i + c_inv))
        y_para.append(a_para * (i + c_para)**2)
        y_exp.append(1 / exp(a_exp * i + c_exp))
        y_lin.append(a_lin * i + c_lin)
    else:
        y_inv.append(burglarylower)
        y_para.append(burglarylower)
        y_exp.append(burglarylower)
        y_lin.append(burglarylower)

plt.plot(plsglobal,y_inv, label="Inverse")
plt.plot(plsglobal,y_para, label="Parabolic")
plt.plot(plsglobal,y_exp, label="Exponential")
plt.plot(plsglobal,y_lin, label="Linear")
plt.legend()
plt.show()
