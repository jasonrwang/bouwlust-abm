---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Bouwlust ABM

This model is to analyze the data generated from experiments using `final_practical/final_practical.nlogo`.

Note: This file needs to be converted into a notebook with jupytext using `jupytext --to notebook analysis/analysis.py`!

```python
# Load dependencies
import pandas as pd
import seaborn as sns
from ema_workbench.analysis import (feature_scoring,pairs_plotting)
from ema_workbench.analysis.scenario_discovery_util import RuleInductionType
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from ema_workbench.analysis import prim
```

## Import Data

```python
data = pd.read_csv('data/final_practical experiment-table.csv',skiprows=6)
```

## Analyze Data

```python
len(data)
```

```python
data.head()
```

## Adjusting Money 

```python
data['adjust_money'] = data['money']-10*data['police-number']-5*data['communityworker']-4*data['garbage-collectors']
len(data[data['adjust_money']>0])
```

### Feature score between inputs and outputs for final values of output metrics

```python
final_pls = data[data['[step]']==4380]
#final_pls = final_pls[final_pls['adjust_money']>0]
inputs = final_pls[['police-number','communityworker','garbage-collectors','pls_QRCode','pls_see_police_comworker','pls_initial_decrease_litter']]
outputs = final_pls[['money','plsglobal']]
fs = feature_scoring.get_feature_scores_all(inputs,outputs,mode=RuleInductionType.REGRESSION)
plt.figure(figsize = (6,8))
sns.heatmap(fs, cmap='viridis', annot=False)
plt.yticks(va="center")
plt.show()
```

## Boxplots for final values of output for different values of inputs 


### Variation with police-number

```python

fig,ax = plt.subplots(1,2,figsize=(15,5))
#plt.figure(figsize=(10, 5))
plt.subplots_adjust(wspace=0.5, top=0.85)
fig.suptitle('Impact of police number on pls and money', 
             y=0.95, fontsize=12)
sns.boxplot(data=final_pls, x='police-number', y='plsglobal', fliersize=0.5,ax=ax[0])
ax[0].set_ylabel('Final PLS')
ax[0].set_xlabel('Police number')
sns.boxplot(data=final_pls, x='police-number', y='money', fliersize=0.5,ax=ax[1])
ax[1].set_ylabel('Money')
ax[1].set_xlabel('Police number')
#ax.set_title('final_pls for different police number')
#ax.set_ybound(-3e7, 6e8) # zoom-in for better visuals

```

### Variation with community workers

```python

fig,ax = plt.subplots(1,2,figsize=(15,5))
#plt.figure(figsize=(10, 5))
plt.subplots_adjust(wspace=0.5, top=0.85)
fig.suptitle('Impact of pcommunity workers on pls and money', 
             y=0.95, fontsize=12)
sns.boxplot(data=final_pls, x='communityworker', y='plsglobal', fliersize=0.5,ax=ax[0])
ax[0].set_ylabel('Final PLS')
ax[0].set_xlabel('communityworker')
sns.boxplot(data=final_pls, x='communityworker', y='money', fliersize=0.5,ax=ax[1])
ax[1].set_ylabel('Money')
ax[1].set_xlabel('communityworker')
#ax.set_title('final_pls for different police number')
#ax.set_ybound(-3e7, 6e8) # zoom-in for better visuals

```

### Variation with Garbage collectors

```python

fig,ax = plt.subplots(1,2,figsize=(15,5))
#plt.figure(figsize=(10, 5))
plt.subplots_adjust(wspace=0.5, top=0.85)
fig.suptitle('Impact of garbage collectors on pls and money', 
             y=0.95, fontsize=12)
sns.boxplot(data=final_pls, x='garbage-collectors', y='plsglobal', fliersize=0.5,ax=ax[0])
ax[0].set_ylabel('Final PLS')
ax[0].set_xlabel('garbage-collectors')
sns.boxplot(data=final_pls, x='garbage-collectors', y='money', fliersize=0.5,ax=ax[1])
ax[1].set_ylabel('Money')
ax[1].set_xlabel('garbage-collectors')
#ax.set_title('final_pls for different police number')
#ax.set_ybound(-3e7, 6e8) # zoom-in for better visuals

```

## Keeping value of one input fixed and performing experiments across different scenarios 


### Impact of police on PLS 

```python

data.loc[data['police-number']==2,'color'] = 'red'
data.loc[data['police-number']==4,'color'] = 'blue'
data.loc[data['police-number']==7,'color'] = 'green'
plt.figure(figsize=(12,6))
ax = plt.axes()
for i in range (1,288):
    df = data[data['[run number]']==i]
    df = df.reset_index(drop=True)
    plt.plot(df['[step]'].tolist(),df['plsglobal'].tolist(),color = df.loc[0,'color'],linewidth=0.25)
plt.xlabel('timestep',fontsize=15)
plt.ylabel('plsglobal',fontsize=15)
plt.ylim(0,400)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.set_facecolor('lightgray')
colors = ['red','blue','green']
lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
labels = ['2 police officers', '4 police officers', '7 police officers']
plt.legend(lines, labels)
plt.show()
```

### Impact of police on Money

```python

data.loc[data['police-number']==2,'color'] = 'red'
data.loc[data['police-number']==4,'color'] = 'blue'
data.loc[data['police-number']==7,'color'] = 'green'
plt.figure(figsize=(12,6))
ax = plt.axes()
for i in range (1,288):
    df = data[data['[run number]']==i]
    df = df.reset_index(drop=True)
    plt.plot(df['[step]'].tolist(),df['money'].tolist(),color = df.loc[0,'color'])
plt.xlabel('timestep',fontsize=15)
plt.ylabel('money',fontsize=15)
plt.ylim(0,120)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.set_facecolor('lightgray')
colors = ['red','blue','green']
lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
labels = ['2 police officers', '4 police officers', '7 police officers']
plt.legend(lines, labels)
plt.show()
```

### Impact of community workers on PLS

```python

data.loc[data['communityworker']==2,'color'] = 'red'
data.loc[data['communityworker']==6,'color'] = 'blue'
data.loc[data['communityworker']==10,'color'] = 'green'
data.loc[data['communityworker']==14,'color'] = 'orange'
plt.figure(figsize=(12,6))
ax = plt.axes()
for i in range (1,288):
    df = data[data['[run number]']==i]
    df = df.reset_index(drop=True)
    plt.plot(df['[step]'].tolist(),df['plsglobal'].tolist(),color = df.loc[0,'color'],linewidth=0.25)
plt.xlabel('timestep',fontsize=15)
plt.ylabel('plsglobal',fontsize=15)
plt.ylim(0,400)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.set_facecolor('lightgray')
colors = ['red','blue','green','orange']
lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
labels = ['2 community workers', '6 community workers', '10 community workers','14 community workers']
plt.legend(lines, labels)
plt.show()
```

### Impact of community workers on money

```python
data.loc[data['communityworker']==2,'color'] = 'red'
data.loc[data['communityworker']==6,'color'] = 'blue'
data.loc[data['communityworker']==10,'color'] = 'green'
data.loc[data['communityworker']==14,'color'] = 'orange'
plt.figure(figsize=(12,6))
ax = plt.axes()
for i in range (1,288):
    df = data[data['[run number]']==i]
    df = df.reset_index(drop=True)
    plt.plot(df['[step]'].tolist(),df['money'].tolist(),color = df.loc[0,'color'])
plt.xlabel('timestep',fontsize=15)
plt.ylabel('money',fontsize=15)
plt.ylim(0,120)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.set_facecolor('lightgray')
colors = ['red','blue','green','orange']
lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
labels = ['2 community workers', '6 community workers', '10 community workers','14 community workers']
plt.legend(lines, labels)
plt.show()
```

### Impact of garbage-collectors on PLS

```python
 
data.loc[data['garbage-collectors']==2,'color'] = 'red'
data.loc[data['garbage-collectors']==5,'color'] = 'blue'
#data.loc[data['garbage-collectors']==8,'color'] = 'green'
plt.figure(figsize=(12,6))
ax = plt.axes()
for i in range (1,288):
    df = data[data['[run number]']==i]
    df = df.reset_index(drop=True)
    plt.plot(df['[step]'].tolist(),df['plsglobal'].tolist(),color = df.loc[0,'color'],linewidth=0.25)
plt.xlabel('timestep',fontsize=15)
plt.ylabel('plsglobal',fontsize=15)
plt.ylim(0,400)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.set_facecolor('lightgray')
colors = ['red','blue']
lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
labels = ['2 garbage collectors', '5 garbage collectors']
plt.legend(lines, labels)
plt.show()
```

### Impact of garbage-collectors on Money

```python

data.loc[data['garbage-collectors']==2,'color'] = 'red'
data.loc[data['garbage-collectors']==5,'color'] = 'blue'
data.loc[data['garbage-collectors']==8,'color'] = 'green'
plt.figure(figsize=(12,6))
ax = plt.axes()
for i in range (1,288):
    df = data[data['[run number]']==i]
    df = df.reset_index(drop=True)
    plt.plot(df['[step]'].tolist(),df['money'].tolist(),color = df.loc[0,'color'])
plt.xlabel('timestep',fontsize=15)
plt.ylabel('money',fontsize=15)
plt.ylim(0,120)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.set_facecolor('lightgray')
colors = ['red','blue']
lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
labels = ['2 garbage collectors', '5 garbage collectors']
plt.legend(lines, labels)
plt.show()
```

## Scenario discovery.Cases for which final money<25

```python

final_pls['fail'] = 0
final_pls.loc[final_pls['money']<25,'fail'] = 1
x = final_pls[['police-number','garbage-collectors','communityworker']]
x = x.reset_index(drop=True)
y = final_pls.loc[:,'fail']
y = y.reset_index(drop=True)
#y = y.values
prim_alg = prim.Prim(x, y, threshold=0.1, peel_alpha=0.1)
box1 = prim_alg.find_box()
box1.show_tradeoff()
plt.show()
box1.inspect(style='graph')
plt.show()
```

## Pairplot for tradeoff between outcomes

```python
final_pls = final_pls.reset_index(drop=True)
sns.pairplot(final_pls, vars=["plsglobal","money"],hue="police-number",diag_kind='hist')
fig.set_size_inches([20, 20])
```

## Impossible scenarios

```python
final_pls['fail'] = 0
final_pls.loc[final_pls['adjust_money']<0,'fail'] = 1
x = final_pls[['police-number','garbage-collectors','communityworker']]
x = x.reset_index(drop=True)
y = final_pls.loc[:,'fail']
y = y.reset_index(drop=True)
#y = y.values
prim_alg = prim.Prim(x, y, threshold=0.1, peel_alpha=0.1)
box1 = prim_alg.find_box()
box1.show_tradeoff()
plt.show()
box1.inspect(style='graph')
plt.show()
```

### plotting PLS for realistic scenarios

```python
real_scen = final_pls[final_pls['fail']==0]
run_num = real_scen['[run number]'].tolist()
real_scen_data = data[data['[run number]'].isin(run_num)]
real_scen_data.loc[real_scen_data['garbage-collectors']==2,'color'] = 'red'
real_scen_data.loc[real_scen_data['garbage-collectors']==5,'color'] = 'blue'
plt.figure(figsize=(12,6))
ax = plt.axes()
for i in range (1,9):
    df = real_scen_data[real_scen_data['[run number]']==run_num[i]]
    df = df.reset_index(drop=True)
    plt.plot(df['[step]'].tolist(),df['plsglobal'].tolist(),color = df.loc[0,'color'],linewidth=0.2)
plt.xlabel('timestep',fontsize=15)
plt.ylabel('plsglobal',fontsize=15)
plt.ylim(0,200)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.set_facecolor('lightgray')
colors = ['red','blue']
lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
labels = ['4 police,2 community workers and 2 garbage collectors','4 police,2 community workers and 5 garbage collectors']
plt.legend(lines, labels)
plt.show()
```
