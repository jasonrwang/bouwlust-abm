__includes [ "utilities.nls" ] ; all the boring but important stuff not related to content
extensions [ csv ]

globals [
  ; variables for the map
  topleftx
  toplefty
  bottomrightx
  bottomrighty

  ; People variables
  percent-with-children
  percent-with-job
  percent-religious
  percent-in-initiatives

  ; Coordinate list of important locations
  patches_in_boundary
  residential_patches
  interaction_patches
  list_initiative_location
  list_place_of_worship
  list_school
  list_supermarket
  list_workplace
  station ; Police Stations
  center ; Community Centre
  list_problem_youth_places
  list_problem_youth_places_hotspot ; Keeps track of active hotspots between spawning and litter functions
  list_problem_youth_places_nothotspot

  prob-creating-initiative

  ; Agent and patch sets
  number-initiatives
  people_with_child_job_religion
  people_with_child_job_noreligion
  people_with_child_nojob_religion
  people_with_child_nojob_noreligion
  people_with_nochild_job_religion
  people_with_nochild_job_noreligion
  people_with_nochild_nojob_religion
  people_with_nochild_nojob_noreligion
  people_burglarized

  ; True Globals
  money
  day_counter
  plsglobal
  pls_upper_threshold

  ; Variables for testing
  pls_initiative_create
  pls_initiative_visit
  pls_burglarized
  pls_burglarized_see_police
  pls_see_police_comworker
  pls_QRCode
  pls_problemyouth
  litter-cleaned-per-collector
  pls_initial_decrease_litter
  pls_initial_increase_people_interaction
  pls_first_person
  pls_second_person
  pls_initial_decrease_problem_youth
  times_problem_youth_temp
  times_interaction_temp
  pls_decrease_problem_youth_temp
  num_interact
]
;turtles-own [initial_xcor final_xcor initial_ycor final_ycor tstep tstep_completed]
; Breeding turtles
breed [ people person ]
breed [ pofficers pofficer ]
breed [ comworkers comworker ]

; Properties of different breeds
pofficers-own [
  ptrouble ; Target location for pofficers on their first day
]
comworkers-own[
  target ; Target location for comworkers on their first day
]
patches-own [
  location ; the string holding the name of this location, default is 0
  category ; string holding the category of the location, default is 0
  litter ; boolean to indicate if there is litter or not, default is false, set in setup-patch-initial-properties
  problem-youth; integer variable for number of problem youth on patch
  pcolor-original ; float carrying original colour of the patch, set in setup-patch-initial-properties
  QRCode  ; boolean to indicate if there is a qr code or not, default is false, set in setup-QRCodes
  initiative-viability ; variable to indicate how strong the initiative is
  number-visits ; variable to keep track of the number of times an initiative is visited, it affetcs its viability
  starting-time ; to save the tick at which the initiative started
]

people-own [
  has-religion
  has-children
  has-job
  has-initiatives
  pls
  number-qrscanned
  initiatives-started
  initiative
  home_location
  nearest_school
  workplace
  nearest_supermarket
  nearest_place_of_worship
  nearest_initiative_location
  visits_to_supermarket
  times_walk
  visits_to_religious
  visits_to_initiatives
  probability_activities
  times_litter_seen
  pls_decrease_by_litter
  times_people_interaction
  times_police_interaction
  pls_increase_people_interaction
  times_problem_youth_seen
  pls_decrease_by_problem_youth
  burglarized-counter ; integer that resets after two weeks (a memory variable)
  burglarized-times ; integer to count times burglarized
  burglarized-police-visits ; integer to count if police have visited people
]

; Initializations and setups
to set-globals
  ; People Properties
  set percent-with-children 37
  set percent-with-job 60
  set percent-religious 50
  set percent-in-initiatives 12
  set money 100
  set number-initiatives 5
  set plsglobal random-normal 40 7
  set pls_upper_threshold 1000

  ;; Define patch sets
  set patches_in_boundary (patches with [pcolor != 126 and pcolor != 44.7 and pcolor != 14.9])
  set residential_patches (patches with [pcolor = 7.9])
  set interaction_patches (patches with [category = "community centre" or category = "supermarket" or category = "religious" or category = "neighbourhood inititiative"])
  ; These are singular locations
  set station one-of (patches with [category = "police station"])
  set center one-of (patches with [category = "community centre"])
  ; These are lists of locations
  set list_workplace (patches with [pcolor = 14.9])
  set list_school (patches with [category = "school"])
  set list_supermarket (patches with [category = "supermarket"])
  set list_initiative_location (patches with [category = "neighbourhood initiative"])
  set list_place_of_worship (patches with [category = "religious"])
  set list_problem_youth_places (patches with [category = "school" or category = "supermarket"])
  ; Keep track of only people who have been burglarized
  set people_burglarized no-turtles

  ; Define PLS change values
  set pls_initial_decrease_litter -5
  set pls_initiative_create 80
  set pls_initiative_visit 10
  set pls_burglarized -80
  set pls_burglarized_see_police 40
  set pls_see_police_comworker 20
  set pls_QRCode [0 8 20] ; Zero, medium, and high values to be used by a random distribution
  set pls_problemyouth -20
  set pls_initial_increase_people_interaction 5
  set pls_initial_decrease_problem_youth -20
  set pls_first_person 0
  set pls_second_person 0
  set times_problem_youth_temp 0
  set pls_decrease_problem_youth_temp 0
  set times_interaction_temp 0
  set num_interact 0
  set day_counter 0

  set litter-cleaned-per-collector 200 ; this number is arbitrary at the moment

  set prob-creating-initiative 5
end

to initialize-pofficers
  create-pofficers police-number [
    setxy 279 395
    set color yellow
    set size 2
    set ptrouble one-of patches_in_boundary ; Destination to walk on the first day
  ]
end

to initialize-comworkers
  create-comworkers communityworker [
    setxy 306 551
    set color green
    set size 2
    set target one-of list_initiative_location
  ]

end

to setup-QRCodes
  ask patches_in_boundary with [category = 0]
    [ set QRCode true ]
end

to setup-initiative-viability
  ask list_initiative_location
    [ set initiative-viability 10  ; to be changed once we agree on a criteria/measure
      set number-visits 0
      set starting-time 0
  ]
end

to setup-people

  create-people initial_number_people [

    ; Define attributes (distribute people types)
    ifelse (random 100 < percent-religious)
      [set has-religion true]
      [set has-religion false]
    ifelse (random 100 < percent-with-children)
      [set has-children true]
      [set has-children false]
    ifelse (random 100 < percent-with-job)
      [set has-job true]
      [set has-job false]
    ifelse (random 100 < percent-in-initiatives)
      [set has-initiatives true]
      [set has-initiatives false]

    ; Given people a home and spawn them there
    set home_location one-of residential_patches
    setxy [pxcor] of home_location [pycor] of home_location

    set workplace min-one-of list_workplace [distance myself]
    set nearest_school min-one-of list_school [distance myself]
    set nearest_supermarket min-one-of list_supermarket [distance myself]
    set nearest_initiative_location min-one-of list_initiative_location [distance myself]
    set nearest_place_of_worship min-one-of list_place_of_worship [distance myself]

    ; Inititalize Counting Variables
    set initiative patch-here
    set visits_to_supermarket 0
    set times_walk 0
    set visits_to_religious 0
    set visits_to_initiatives 0
    set pls random-normal 40 7 ; Set initial PLS
    set times_litter_seen 0
    set pls_decrease_by_litter 0
    set times_people_interaction 0
    set pls_increase_people_interaction 0
    set times_problem_youth_seen 0
    set pls_decrease_by_problem_youth 0
    set times_police_interaction 0

    set burglarized-counter 0;
    set burglarized-times 0;
    set burglarized-police-visits 0;
  ]
  set people_with_child_job_religion people with [(has-children = true) and (has-job = true) and (has-religion = true)]
  set people_with_child_job_noreligion people with [(has-children = true) and (has-job = true) and (has-religion = false)]
  set people_with_child_nojob_religion people with [(has-children = true) and (has-job = false) and (has-religion = true)]
  set people_with_child_nojob_noreligion people with [(has-children = true) and (has-job = false) and (has-religion = false)]
  set people_with_nochild_job_religion people with [(has-children = false) and (has-job = true) and (has-religion = true)]
  set people_with_nochild_job_noreligion people with [(has-children = false) and (has-job = true) and (has-religion = false)]
  set people_with_nochild_nojob_religion people with [(has-children = false) and (has-job = false) and (has-religion = true)]
  set people_with_nochild_nojob_noreligion people with [(has-children = false) and (has-job = false) and (has-religion = false)]

  ask people_with_child_job_religion [
    set probability_activities 0.5454 ;6/11
  ]
  ask people_with_child_job_noreligion [
    set probability_activities 0.4545  ;5/11
  ]
  ask people_with_child_nojob_religion [
    set probability_activities 0.5454  ;6/11
  ]
  ask people_with_child_nojob_noreligion [
    set probability_activities 0.4545  ;5/11
  ]
  ask people_with_nochild_job_religion [
    set probability_activities 0.5454  ;6/11
  ]
  ask people_with_nochild_job_noreligion [
    set probability_activities 0.4545  ;5/11
  ]
  ask people_with_nochild_nojob_religion [
    set probability_activities 0.2857  ;6/21
  ]
  ask people_with_nochild_nojob_noreligion [
    set probability_activities 0.2381   ;5/21
  ]
end

to setup-patch-initial-properties
  ask patches_in_boundary [
    set pcolor-original pcolor
    set litter false
    set problem-youth 0
  ]
end

to setup

  clear-all
  reset-ticks

  setupMap ; This must load before the others
  loadData

  set-globals
  setup-people
  initialize-pofficers
  initialize-comworkers
  setup-patch-initial-properties
  setup-initiative-viability
  setup-QRCodes

  ; Turned off for headless optimization
  ; setup-plot ; Sets up plots

end

; Go function
to go
  ; Initialize tick
  tick

  create-burglary
  update-burglaries
  create-problem-youth ; problem youth create litter, so ensure order is right
  create-litter
  clean-litter ; cleans litter too every week

  ask pofficers [pofficers-schedule]
  ask comworkers [comworkers-schedule]
  citizen_schedule_people_with_child_job_religion
  citizen_schedule_people_with_child_job_noreligion
  citizen_schedule_people_with_child_nojob_religion
  citizen_schedule_people_with_nochild_job_religion
  citizen_schedule_people_with_nochild_nojob_religion
  citizen_schedule_people_with_nochild_job_noreligion
  citizen_schedule_people_with_child_nojob_noreligion
  citizen_schedule_people_with_nochild_nojob_noreligion

  ; Functions
  ask people
    [ pls-degradation ]
  viability-degradation
  ending-initiatives
  effect_litter_on_pls
  effect_interaction_on_pls
  effect_problem_youth_pls
  globals-update
  pls-calc

  ; Toggle off in interface for faster operation in headless mode
  if plot? [ do-plot ] ; Updates plots

end


to pls-calc
  set plsglobal (mean [pls] of people - (standard-deviation [pls] of people) / (mean [pls] of people) )
end

to pofficers-schedule
  if ( ticks mod 4 = 1 or ticks mod 4 = 2)
    [ move-pofficer-to-trouble
      police-interaction]
  if ( ticks mod 4 = 3 or ticks mod 4 = 0)
    [ return-officers-workers
      police-interaction
    ]
end

to comworkers-schedule
  if ( ticks mod 4 = 1 or ticks mod 4 = 2)
    [ move-comworker-toinitiatives
      comworker-interaction]
  if ( ticks mod 4 = 3 or ticks mod 4 = 0)
    [ return-officers-workers
      comworker-interaction]
end

to move-pofficer-to-trouble
  ask pofficers [
    if ( ticks mod 4 = 1 )[
      ; Beginning of the day – pick destination and go partway

      ; Go to trouble, favouring problem youth with 90%
      let n-hotspots min list 4 (count list_problem_youth_places_hotspot with [problem-youth > 0])
      ifelse (random 100 < 90)
        [set ptrouble one-of (max-n-of n-hotspots list_problem_youth_places_hotspot [problem-youth])] ; Pick one of up to four places with maximum problem-youth
        [set ptrouble one-of (patches_in_boundary with [litter = true])] ; Pick some random litter patch

      ; Prioritize going to people who have been burglarized and overwrite ptrouble
      let people-need-visit (people_burglarized with [burglarized-police-visits < burglarized-times])
      if any? people-need-visit [; Has anyone been burglarized and NOT visited by police?
        if (random 100 < 80) ; 80% Chance police actually go (represents underreporting or other effects)
          [set ptrouble one-of people-need-visit]
      ]

      ; If there are no litter, problem youth, or burglaries, then go to a random spot.
      if ptrouble = nobody [set ptrouble one-of patches_in_boundary]

      ; Face and go part of the way
      face ptrouble
      forward ((random-float 1) * (distance ptrouble))
    ]
    if ticks mod 4 = 2 [
      ; By noon, arrive at destination
      move-to ptrouble
      police-handling
      if member? ptrouble people_burglarized [ ; Change PLS for people police visit (easier this way than in police-handling function)
        ask ptrouble [
          set pls pls + pls_burglarized_see_police
          set burglarized-police-visits (burglarized-police-visits + 1)
        ]
      ]
    ]
  ]
end

to police-handling
  ask pofficers [ ; Must be expanded to include burglaries
    if (litter = true) [ set litter false ]
    if (problem-youth > 0) [ move-problem-youth ]
  ]
end

to move-problem-youth
  let list_problem_youth_places_nopolice list_problem_youth_places with [not any? pofficers-here]
  let moving-youth problem-youth
  ask one-of list_problem_youth_places_nopolice [ set problem-youth (problem-youth + floor (moving-youth * 0.8) )] ; 80% of youth in old spot move to another spot
  set problem-youth 0 ; Set problem-youth in where police are to be zero
end


to move-comworker-toinitiatives
 ask comworkers [
    if (ticks mod 4 = 1) [
      set target one-of (list_initiative_location with [not any? comworkers-here])
      if (target = NOBODY)
        [ set target center ]
      face target
      forward ((random-float 1) * (distance target))
    ]
    if ticks mod 4 = 2 [
      face target
      move-to target
    ]
  ]
end

to return-officers-workers
  ; Returns police officers and community works
  ask pofficers [
    if (ticks mod 4 = 3) [
      face station
      forward ((random-float 1) * (distance station))
    ]
    if (ticks mod 4 = 0) [
      face station
      move-to station
    ]
  ]
  ask comworkers [
    if (ticks mod 4 = 3)
       [ face center
         forward ((random-float 1) * (distance center))
       ]
    if ticks mod 4 = 0
      [ face center
        move-to center
      ]
  ]
end

to effect-initiatives
  ask people [
    if [category] of patch-here = "neighbourhood initiative" [
      ask patch-here [
        set initiative-viability (initiative-viability + 1)
        set number-visits (number-visits + 1)
        set pcolor blue
      ]
      set pls (pls + pls_initiative_visit)
    ]
  ]
  ask comworkers [
    if ([category] of patch-here = "neighbourhood initiative")
      [set initiative-viability (initiative-viability + 3)]
  ]
end

to comworker-interaction
  ask people in-radius 4
    [ set pls (pls + pls_see_police_comworker) ]
end

to police-interaction
  ask pofficers [
    ask people with [distance myself <= 10]
    [ set pls (pls + pls_see_police_comworker)
      set times_police_interaction times_police_interaction + 1]
  ]
end

; Citizens create initiatives following a probability inversely proportional to the pls
to create-initiatives
  if (number-initiatives < 10 and category = 0 and communityworker > 0) [
    if ((random-float 100 < prob-creating-initiative ) or number-qrscanned > 20)
      [ask patch-here [
        set category "neighbourhood initiative"
        set starting-time ticks
        set list_initiative_location (patch-set list_initiative_location self) ; Add to patchset
       ]
      ]
      set initiatives-started (initiatives-started + 1)
      set pls (pls + pls_initiative_create)
      set number-initiatives (number-initiatives + 1)
      set money (money - 2)
  ]

end

; People can scan QR codes, everytime they do, it has an impact on their pls.
; This only occurs when citizens go for walks
to scan-QR
  ifelse(number-qrscanned > 21)
    [set number-qrscanned 0.8 * number-qrscanned]
    [ let qrcodesaround count (patches in-radius 2) with [QRCode = true]
      set number-qrscanned  (number-qrscanned + (qrcodesaround))
       set pls (pls + one-of pls_QRCode)
       ]
end

to viability-degradation   ; every week, the viability of an initiative decreases by 25 %
ask list_initiative_location
  [ if ((ticks - starting-time) = 28)
    [ set initiative-viability (round (0.8 * initiative-viability))
    ]
  ]
end


; At the end of the week, pls decreases by 25%, accounting for the effect of memory
to pls-degradation
    set pls (round (0.98 * pls))
end


; Initiatives last 6 months
; Initiatives with viability lower than the mean viability after 2 weeks die out
to ending-initiatives
  if not any? list_initiative_location [ stop ]
  let initiative-viability-mean (mean [initiative-viability] of list_initiative_location)
  ask list_initiative_location [
    if ((ticks - starting-time) = 720) or ( (ticks - starting-time) = 112 and initiative-viability < initiative-viability-mean) [
      set category 0   ; to end the initiative
      set number-initiatives (number-initiatives - 1)
      set list_initiative_location other list_initiative_location ; Remove this location from patchset
    ]
  ]
end

; Update global variables
to globals-update
  ; This is to update the probability of creating an initiative using an inverse proportional function with the global PLS
  ifelse ( plsglobal < 75 )
    [ set prob-creating-initiative prob-creating-initiative + 20]
    [
      let prob-creating-upper 20
      let prob-creating-lower 5
      let a ln (prob-creating-upper / prob-creating-lower) / pls_upper_threshold
      let c ln (1 / (exp (a * pls_upper_threshold) * prob-creating-lower)) ; an inversely proportional litter-proportion=a/(aggregate-pls +c) relationship
      ifelse (plsglobal < pls_upper_threshold)
        [set prob-creating-initiative (1 / exp (a * plsglobal + c))]
        [set prob-creating-initiative (prob-creating-lower)]
    ]

  if (prob-creating-initiative < 0)
    [ set prob-creating-initiative 0 ]
  if ( prob-creating-initiative > 100)
    [ set prob-creating-initiative 100 ]

  ; This is to keep track of the days in order to implement a schedule for citizens.
  if (ticks > 0 and ticks mod 4 = 0)
    [ set day_counter (day_counter + 1) ]

  if (money < 0)
    [ set money 0 ]
end

to create-litter
  ; Function to create litter at every tick
  ; Boundary conditions for the relationship between plsglobal and amount of litter created
  ; the litter value at 0 plsglobal is litter-upper and litter-lower at the pls_upper_threshold plsglobal value
  ; if PLS is above the pls_upper_threshold, litter-lower will still be the amount of tiles where garbage appears.

  let litter-upper 0.05 / (365 * 4)
  let litter-lower 0.000001 / (365 * 4)

  let a ln (litter-upper / litter-lower) / pls_upper_threshold
  let c ln (1 / (exp (a * pls_upper_threshold) * litter-lower)) ; an inversely proportional litter-proportion=a/(plsglobal +c) relationship
  let litter-proportion 0 ; initialize the variable
  ifelse (plsglobal < pls_upper_threshold)
    [set litter-proportion (1 / exp (a * plsglobal + c))]
    [set litter-proportion litter-lower]

  ; Change random set of patches
  let num-patches-to-change litter-proportion * count patches_in_boundary * 20
  ask up-to-n-of num-patches-to-change patches_in_boundary [ set litter true ]

  ; Created by problem youth
  ask list_problem_youth_places_hotspot [
    let litter-zone (patches in-radius 3) ; 3 patch radius, which is 27 patches
    ;print(count people-on litter-zone)
    let new-litter (problem-youth / 20 * 27 * 0.5); Proportional to the number of youth at the hotspot, with a max of half of the patches
    ask up-to-n-of new-litter litter-zone [ set litter true ] ; This will ask patches that already have litter, which is ok
  ]
end

to clean-litter
  ; Proportional to number of garbage collectors, happens once a week

  let num-patches-to-change (litter-cleaned-per-collector * garbage-collectors)

  ; Since tick size is still undecided, this value needs to change
  if (ticks mod 50 = 0) ; true for a whole week
    [
      let littered-patches patches_in_boundary with [litter = true]
      ask up-to-n-of num-patches-to-change littered-patches [set litter false] ; Sometimes, there might not be as much litter as collectors!
    ]
end

to create-burglary
  ;; Create a new burglary
  ; Burglaries only happen once a day
  if not (ticks mod 4 = 1) [ stop ]

  ; Determine likelihood for burglary based on PLS
  ; Follows same logic as create-litter's inversely proportional relation... for now
  let burglary-rate-year-worst 0.10 ; (%) Twice the worst case burglary rate per year (Chile)
  let burglary-rate-year-best 0.00019 ; (%) Best burglary rate in high GDP world (Singapore)
  ; Ignore agent:population scaling factors
  let burglary-upper (burglary-rate-year-worst / 365 ); * initial_number_people / 27505) ; Worst case burglary rate per day
  let burglary-lower (burglary-rate-year-best / 365 ) ; * initial_number_people / 27505) ; Worst case burglary rate per day

  let a ln (burglary-upper / burglary-lower) / pls_upper_threshold
  let c ln (1 / (exp (a * pls_upper_threshold) * burglary-lower))
  let burglary-probability 0 ; initialize the variable
  ifelse (plsglobal < pls_upper_threshold)
    [set burglary-probability (1 / exp (a * plsglobal + c))]
    [set burglary-probability burglary-lower]

  ; Burglarize random set of agents
  ask people [
    let prob burglary-probability * 1e5
    let number random-float 1 * 1e5

    if number <= prob [
      set burglarized-counter (burglarized-counter + 15) ; Forget they've been burglarized after 14 days. Start with 15 since 1 is substracted immediately after in update-burglaries
      set burglarized-times (burglarized-times + 1)
      set pls (pls + pls_burglarized) ; Add because variable is negative

      ; Update the agentset of people who have been burglarized with new
      set people_burglarized (turtle-set people_burglarized self)
    ]
  ]
end

to update-burglaries
  ; Countdown for how long people remember a burglary that happened to them
  ask people_burglarized [
    if burglarized-counter > 0 [ set burglarized-counter (burglarized-counter - 1) ] ; Don't go below 0
  ]
end

to create-problem-youth
  ; Function to spawn-problem-youth problem youth at every tick
  ; Boundary conditions for the relationship between plsglobal and number of problem youth that spawn-problem-youth
  ; the spawn-problem-youth value at 0 plsglobal is spawn-problem-youth-upper and spawn-problem-youth-lower at the pls_upper_threshold plsglobal value
  ; if PLS is above the pls_upper_threshold, spawn-problem-youth-lower will still be the amount of tiles where garbage appears.
  let spawn-problem-youth-upper 8
  let spawn-problem-youth-lower 0.1 ; Can't be zero!
  let a ln (spawn-problem-youth-upper / spawn-problem-youth-lower) / pls_upper_threshold
  let c ln (1 / (exp (a * pls_upper_threshold) * spawn-problem-youth-lower)) ; a negative exponential proportional spawn-problem-youth-number=1/exp(a * plsglobal +c) relationship
  let spawn-problem-youth-number 0 ; initialize the variable
  ifelse (plsglobal < pls_upper_threshold)
    [set spawn-problem-youth-number (1 / exp (a * plsglobal + c))]
    [set spawn-problem-youth-number spawn-problem-youth-lower]

  update-problem-youth-places

  ; Spawn problem youth in existing hotspots or new ones
  let loop_counter floor spawn-problem-youth-number ; Create a counter
  while [ loop_counter > 0 ] [
    ; Allocate youth one at a time
    set loop_counter (loop_counter - 1)

    if (not any? list_problem_youth_places_hotspot) [ ; When there are no problem spots, spawn only in a place without youth
      ask one-of list_problem_youth_places [ set problem-youth problem-youth + 1 ]
    ]
    if (not any? list_problem_youth_places_nothotspot) [ ; When every possible hotspot has problem youth
      ask one-of list_problem_youth_places [ set problem-youth problem-youth + 1 ]
    ]
    if (any? list_problem_youth_places_hotspot and any? list_problem_youth_places_nothotspot) [
      ; Runs only if there is already a hotspot but not all locations
      ifelse random 100 <= 85 ; 85% bias to joining an existing hotspot
        [ask one-of list_problem_youth_places_hotspot [ set problem-youth problem-youth + 1 ]]
        [ask one-of list_problem_youth_places_nothotspot [ set problem-youth problem-youth + 1 ]]
    ]
    update-problem-youth-places
  ]

  ; While there are too many problem youth – adds another soft negative feedback loop
  while [ count list_problem_youth_places with [problem-youth > 20] > 0 ] [
    ask list_problem_youth_places with [problem-youth > 20] [
      let proportion_to_leave random one-of [25 50 75] ; Randomly generate some proportion (a group) of problem youth to split from the larger group
      let proportion_to_go_home 0.2 ; Some people just go home when group splits
      let num_to_leave round (problem-youth * proportion_to_leave / 100) ; Turn above proportion into an integer
      set problem-youth (problem-youth - num_to_leave) ; The above number leave the current patch
      let num_to_arrive round (num_to_leave * (1 - proportion_to_go_home))
      ifelse any? list_problem_youth_places_nothotspot
        [ifelse random 100 <= 85 ; 85% bias to joining an existing hotspot
          [ask one-of list_problem_youth_places_hotspot [set problem-youth (problem-youth + num_to_arrive)]] ; The above number arrives in a new patch
          [ask one-of list_problem_youth_places_nothotspot [set problem-youth (problem-youth + num_to_arrive)]]
        ]
        [ifelse any? (list_problem_youth_places_hotspot with [problem-youth < 20])
          [ask one-of (list_problem_youth_places_hotspot with [problem-youth < 20]) [set problem-youth (problem-youth + num_to_arrive)]]
          [ask one-of list_problem_youth_places [set problem-youth (problem-youth + num_to_arrive)] ] ; If all places full, youth are simply redistributed and some go home (this creates a cap of 17 * 20 problem youth in the model)
        ]
    ]
    update-problem-youth-places
  ]
end

to update-problem-youth-places
  ; Create/update a list of hotspot and not hotspot places
  set list_problem_youth_places_hotspot (list_problem_youth_places with [problem-youth > 0])
  set list_problem_youth_places_nothotspot (list_problem_youth_places with [problem-youth = 0]) ; Get list of places that are current hotspots
end

to citizen_schedule_people_with_child_job_religion
  if (day_counter mod 7 != 5 and day_counter mod 7 != 6)[
     ask people_with_child_job_religion[

       if ticks mod 4 = 1 [
          ifelse random 100 < 50
            [ face workplace
              forward ((random-float 1) * (distance workplace))]
            [ move-to nearest_school]
          ]

        if ticks mod 4 = 2 [
             move-to workplace
        ]

        if ticks mod 4 = 3 [
          ifelse (random 100 < 50)
            [ifelse (random-float 1 < probability_activities)
              [decide_activities_religious_people]
              [ face home_location
                forward ((random-float 1) * (distance home_location))
              ]
            ]
            [ move-to nearest_school]
        ]

        if ticks mod 4 = 0[
           move-to home_location

        ]
      ]
    ]

  if (day_counter mod 7 = 5)[
     ask people_with_child_job_religion[
      if ticks mod 4 = 1[
        ifelse random 100 < 12
          [ move-to nearest_initiative_location
            set visits_to_initiatives visits_to_initiatives + 1]
          [ ifelse (random-float 1 < probability_activities)
              [decide_activities_religious_people]
              [move-to home_location]
          ]
      ]
      if (ticks mod 4 = 2 or ticks mod 4 = 3)[
            ifelse (random-float 1 < probability_activities)
              [decide_activities_religious_people]
              [move-to home_location]
     ]

      if ticks mod 4 = 0[
        move-to home_location
      ]
     ]
  ]
  if (day_counter mod 7 = 6)[
     ask people_with_child_job_religion[

       if (ticks mod 4 = 1 or ticks mod 4 = 2 or ticks mod 4 = 3)[
              ifelse (random-float 1 < probability_activities)
                [decide_activities_religious_people]
                [move-to home_location]

       ]

       if ticks mod 4 = 0[
         move-to home_location
         set visits_to_supermarket 0
         set times_walk 0
         set visits_to_religious 0
         set visits_to_initiatives 0
       ]
     ]
  ]
end


to decide_activities_religious_people
  if (visits_to_supermarket < 3 and times_walk < 2 and visits_to_religious < 1)[
    ifelse random 100 < 33
     [move-to nearest_place_of_worship
        set visits_to_religious visits_to_religious + 1
     ]
     [ifelse random 100 < 50
      [move-to nearest_supermarket
       set visits_to_supermarket visits_to_supermarket + 1]
       [setxy random-xcor random-ycor
        create-initiatives
        scan-QR
        set times_walk times_walk + 1
        while [(member? patch-here patches_in_boundary) = False]
         [setxy random-xcor random-ycor]
        probability_of_interaction_walking

       ]
    ]
  ]

  if (visits_to_supermarket >= 3 and times_walk < 2 and visits_to_religious < 1)[
    ifelse random 100 < 50
     [move-to nearest_place_of_worship
        set visits_to_religious visits_to_religious + 1]
     [setxy random-xcor random-ycor
      create-initiatives
      scan-QR
      set times_walk times_walk + 1
      while [(member? patch-here patches_in_boundary) = False]
       [setxy random-xcor random-ycor]
      probability_of_interaction_walking
    ]
  ]
  if (visits_to_supermarket < 3 and times_walk >= 2 and visits_to_religious < 1)[
    ifelse random 100 < 50
     [move-to nearest_place_of_worship
        set visits_to_religious visits_to_religious + 1]
      [move-to nearest_supermarket
       set visits_to_supermarket visits_to_supermarket + 1]
  ]

  if (visits_to_supermarket < 3 and times_walk < 2 and visits_to_religious >= 1)[
     ifelse random 100 < 50
      [move-to nearest_supermarket
       set visits_to_supermarket visits_to_supermarket + 1]
       [setxy random-xcor random-ycor
        create-initiatives
        scan-QR
        set times_walk times_walk + 1
        while [(member? patch-here patches_in_boundary) = False]
         [setxy random-xcor random-ycor]
        probability_of_interaction_walking
    ]
  ]

  if (visits_to_supermarket >= 3 and times_walk >= 2 and visits_to_religious < 1)[
     move-to nearest_place_of_worship
     set visits_to_religious visits_to_religious + 1]

  if (visits_to_supermarket < 3 and times_walk >= 2 and visits_to_religious >= 1)[
     move-to nearest_supermarket
     set visits_to_supermarket visits_to_supermarket + 1]

  if (visits_to_supermarket >= 3 and times_walk < 2 and visits_to_religious >= 1)[
     setxy random-xcor random-ycor
     create-initiatives
     scan-QR
     set times_walk times_walk + 1
     while [(member? patch-here patches_in_boundary) = False]
       [setxy random-xcor random-ycor]
     probability_of_interaction_walking
  ]
  if (visits_to_supermarket >= 3 and times_walk >= 2 and visits_to_religious >= 1)
     [move-to home_location]
end

to citizen_schedule_people_with_child_job_noreligion
  if (day_counter mod 7 != 5 and day_counter mod 7 != 6)[
     ask people_with_child_job_noreligion[

       if ticks mod 4 = 1 [
          ifelse random 100 < 50
            [ face workplace
              forward ((random-float 1) * (distance workplace))]
            [ move-to nearest_school]
          ]

        if ticks mod 4 = 2 [
             move-to workplace
        ]

        if ticks mod 4 = 3 [
          ifelse (random 100 < 50)
            [ifelse (random-float 1 < probability_activities)
              [decide_activities_nonreligious_people]
              [ face home_location
                forward ((random-float 1) * (distance home_location))]
            ]
            [ move-to nearest_school]
          ]

        if ticks mod 4 = 0[
           move-to home_location

        ]
      ]
    ]

  if (day_counter mod 7 = 5)[
     ask people_with_child_job_noreligion[
      if ticks mod 4 = 1[
        ifelse random 100 < 12
          [ move-to nearest_initiative_location
            set visits_to_initiatives visits_to_initiatives + 1]
          [ ifelse (random-float 1 < probability_activities)
              [decide_activities_nonreligious_people]
              [move-to home_location]
          ]
      ]
      if (ticks mod 4 = 2 or ticks mod 4 = 3)[
            ifelse (random-float 1 < probability_activities)
              [decide_activities_nonreligious_people]
              [move-to home_location]
     ]

      if ticks mod 4 = 0[
        move-to home_location
      ]
     ]
  ]
  if (day_counter mod 7 = 6)[
     ask people_with_child_job_noreligion[

       if (ticks mod 4 = 1 or ticks mod 4 = 2 or ticks mod 4 = 3)[
              ifelse (random-float 1 < probability_activities)
                [decide_activities_nonreligious_people]
                [move-to home_location]

       ]

       if ticks mod 4 = 0[
         move-to home_location
         set visits_to_supermarket 0
         set times_walk 0
         set visits_to_religious 0
         set visits_to_initiatives 0
       ]
     ]
  ]

end

to decide_activities_nonreligious_people

  if (visits_to_supermarket < 3 and times_walk < 2 )[
     ifelse random 100 < 50
      [
        move-to nearest_supermarket
        set visits_to_supermarket visits_to_supermarket + 1
      ]
      [
        setxy random-xcor random-ycor
        create-initiatives
        scan-QR
        set times_walk times_walk + 1
        while [(member? patch-here patches_in_boundary) = False]
          [setxy random-xcor random-ycor]
        probability_of_interaction_walking
    ]
  ]

  if (visits_to_supermarket < 3 and times_walk >= 2 )[
     move-to nearest_supermarket
     set visits_to_supermarket visits_to_supermarket + 1]

  if (visits_to_supermarket >= 3 and times_walk < 2)[
     setxy random-xcor random-ycor
     create-initiatives
     scan-QR
     set times_walk times_walk + 1
     while [(member? patch-here patches_in_boundary) = False]
       [setxy random-xcor random-ycor]
     probability_of_interaction_walking
  ]
  if (visits_to_supermarket >= 3 and times_walk >= 2 )
     [move-to home_location]
end


to citizen_schedule_people_with_child_nojob_religion
  if (day_counter mod 7 != 5 and day_counter mod 7 != 6)[
     ask people_with_child_nojob_religion[

       if ticks mod 4 = 1
         [ move-to nearest_school]

        if ticks mod 4 = 2[
          ifelse random-float 1 < probability_activities
            [ decide_activities_religious_people]
            [move-to home_location]
        ]

        if ticks mod 4 = 3[
           move-to nearest_school
        ]

        if ticks mod 4 = 0[
           move-to home_location
        ]
     ]
  ]

  if (day_counter mod 7 = 5)[
     ask people_with_child_nojob_religion[
      if ticks mod 4 = 1[
        ifelse random 100 < 12
          [ move-to nearest_initiative_location
            set visits_to_initiatives visits_to_initiatives + 1]
          [ ifelse (random-float 1 < probability_activities)
              [decide_activities_religious_people]
              [move-to home_location]
          ]
      ]
      if (ticks mod 4 = 2 or ticks mod 4 = 3)[
            ifelse (random-float 1 < probability_activities)
              [decide_activities_religious_people]
              [move-to home_location]
     ]

      if ticks mod 4 = 0[
        move-to home_location
      ]
     ]
  ]
  if (day_counter mod 7 = 6)[
     ask people_with_child_nojob_religion[

       if (ticks mod 4 = 1 or ticks mod 4 = 2 or ticks mod 4 = 3)[
              ifelse (random-float 1 < probability_activities)
                [decide_activities_religious_people]
                [move-to home_location]

       ]

       if ticks mod 4 = 0[
         move-to home_location
         set visits_to_supermarket 0
         set times_walk 0
         set visits_to_religious 0
         set visits_to_initiatives 0
       ]
     ]
  ]
end


to citizen_schedule_people_with_nochild_job_religion
  if (day_counter mod 7 != 5 and day_counter mod 7 != 6)[
     ask people_with_nochild_job_religion[

      if ticks mod 4 = 1[
       face workplace
       forward ((random-float 1) * (distance workplace))
      ]


       if ticks mod 4 = 2[
         move-to workplace
       ]

       if ticks mod 4 = 3[
         ifelse random-float 1 < probability_activities
          [decide_activities_religious_people]
          [face home_location
           forward ((random-float 1) * (distance home_location))]
       ]

       if ticks mod 4 = 0[
         move-to home_location
       ]

     ]
  ]

  if (day_counter mod 7 = 5)[
     ask people_with_nochild_job_religion[
      if ticks mod 4 = 1[
        ifelse random 100 < 12
          [ move-to nearest_initiative_location
            set visits_to_initiatives visits_to_initiatives + 1]
          [ ifelse (random-float 1 < probability_activities)
              [decide_activities_religious_people]
              [move-to home_location]
          ]
      ]
      if (ticks mod 4 = 2 or ticks mod 4 = 3)[
            ifelse (random-float 1 < probability_activities)
              [decide_activities_religious_people]
              [move-to home_location]
     ]

      if ticks mod 4 = 0[
        move-to home_location
      ]
     ]
  ]
  if (day_counter mod 7 = 6)[
     ask people_with_nochild_job_religion[

       if (ticks mod 4 = 1 or ticks mod 4 = 2 or ticks mod 4 = 3)[
              ifelse (random-float 1 < probability_activities)
                [decide_activities_religious_people]
                [move-to home_location]

       ]

       if ticks mod 4 = 0[
         move-to home_location
         set visits_to_supermarket 0
         set times_walk 0
         set visits_to_religious 0
         set visits_to_initiatives 0
       ]
     ]
  ]
end


to citizen_schedule_people_with_child_nojob_noreligion
  if (day_counter mod 7 != 5 and day_counter mod 7 != 6)[
    ask people_with_child_nojob_noreligion[

        if ticks mod 4 = 1[
         move-to nearest_school
        ]

        if ticks mod 4 = 2[
          ifelse random-float 1 < probability_activities
            [ decide_activities_nonreligious_people]
            [move-to home_location]
        ]

        if ticks mod 4 = 3[
           move-to nearest_school
        ]

        if ticks mod 4 = 0[
          move-to home_location
        ]

    ]
  ]

  if (day_counter mod 7 = 5)[
     ask people_with_child_nojob_noreligion[
      if ticks mod 4 = 1[
        ifelse random 100 < 12
          [ move-to nearest_initiative_location
            set visits_to_initiatives visits_to_initiatives + 1]
          [ ifelse (random-float 1 < probability_activities)
              [decide_activities_nonreligious_people]
              [move-to home_location]
          ]
      ]
      if (ticks mod 4 = 2 or ticks mod 4 = 3)[
            ifelse (random-float 1 < probability_activities)
              [decide_activities_nonreligious_people]
              [move-to home_location]
     ]

      if ticks mod 4 = 0[
        move-to home_location
      ]
     ]
  ]
  if (day_counter mod 7 = 6)[
     ask people_with_child_nojob_noreligion[

       if (ticks mod 4 = 1 or ticks mod 4 = 2 or ticks mod 4 = 3)[
              ifelse (random-float 1 < probability_activities)
                [decide_activities_nonreligious_people]
                [move-to home_location]

       ]

       if ticks mod 4 = 0[
         move-to home_location
         set visits_to_supermarket 0
         set times_walk 0
         set visits_to_religious 0
         set visits_to_initiatives 0
       ]
     ]
  ]

end


to citizen_schedule_people_with_nochild_job_noreligion
  if (day_counter mod 7 != 5 and day_counter mod 7 != 6)[

     ask people_with_nochild_job_noreligion[

      if ticks mod 4 = 1[
       face workplace
       forward ((random-float 1) * (distance workplace))
      ]


       if ticks mod 4 = 2[
         move-to workplace
       ]

       if ticks mod 4 = 3[
         ifelse random-float 1 < probability_activities
          [decide_activities_nonreligious_people]
          [face home_location
           forward ((random-float 1) * (distance home_location))]
       ]

       if ticks mod 4 = 0[
         move-to home_location
       ]

     ]
    ]

  if (day_counter mod 7 = 5)[
     ask people_with_nochild_job_noreligion[
      if ticks mod 4 = 1[
        ifelse random 100 < 12
          [ move-to nearest_initiative_location
            set visits_to_initiatives visits_to_initiatives + 1]
          [ ifelse (random-float 1 < probability_activities)
              [decide_activities_nonreligious_people]
              [move-to home_location]
          ]
      ]
      if (ticks mod 4 = 2 or ticks mod 4 = 3)[
            ifelse (random-float 1 < probability_activities)
              [decide_activities_nonreligious_people]
              [move-to home_location]
     ]

      if ticks mod 4 = 0[
        move-to home_location
      ]
     ]
  ]
  if (day_counter mod 7 = 6)[
     ask people_with_nochild_job_noreligion[

       if (ticks mod 4 = 1 or ticks mod 4 = 2 or ticks mod 4 = 3)[
              ifelse (random-float 1 < probability_activities)
                [decide_activities_nonreligious_people]
                [move-to home_location]

       ]

       if ticks mod 4 = 0[
         move-to home_location
         set visits_to_supermarket 0
         set times_walk 0
         set visits_to_religious 0
         set visits_to_initiatives 0
       ]
     ]
  ]
end

to citizen_schedule_people_with_nochild_nojob_religion
  if (day_counter mod 7 != 5 and day_counter mod 7 != 6)[
     ask people_with_nochild_nojob_religion[

       if ticks mod 4 != 0[
        ifelse random-float 1 < probability_activities
          [decide_activities_religious_people]
          [move-to home_location]
       ]

       if ticks mod 4 = 0[
         move-to home_location
       ]

      ]
    ]

  if (day_counter mod 7 = 5)[
     ask people_with_nochild_nojob_religion[
      if ticks mod 4 = 1[
        ifelse random 100 < 12
          [ move-to nearest_initiative_location
            set visits_to_initiatives visits_to_initiatives + 1]
          [ ifelse (random-float 1 < probability_activities)
              [decide_activities_religious_people]
              [move-to home_location]
          ]
      ]
      if (ticks mod 4 = 2 or ticks mod 4 = 3)[
            ifelse (random-float 1 < probability_activities)
              [decide_activities_religious_people]
              [move-to home_location]
     ]

      if ticks mod 4 = 0[
        move-to home_location
      ]
     ]
  ]
  if (day_counter mod 7 = 6)[
     ask people_with_nochild_nojob_religion[

       if (ticks mod 4 = 1 or ticks mod 4 = 2 or ticks mod 4 = 3)[
              ifelse (random-float 1 < probability_activities)
                [decide_activities_religious_people]
                [move-to home_location]

       ]

       if ticks mod 4 = 0[
         move-to home_location
         set visits_to_supermarket 0
         set times_walk 0
         set visits_to_religious 0
         set visits_to_initiatives 0
       ]
     ]
  ]
end


to citizen_schedule_people_with_nochild_nojob_noreligion
  if (day_counter mod 7 != 5 and day_counter mod 7 != 6)[
     ask people_with_nochild_nojob_noreligion[
       if ticks mod 4 != 0[
        ifelse random-float 1 < probability_activities
          [decide_activities_nonreligious_people]
          [move-to home_location]
       ]

       if ticks mod 4 = 0[
        move-to home_location
       ]

      ]
    ]

  if (day_counter mod 7 = 5)[
     ask people_with_nochild_nojob_noreligion[
      if ticks mod 4 = 1[
        ifelse random 100 < 12
          [ move-to nearest_initiative_location
            set visits_to_initiatives visits_to_initiatives + 1]
          [ ifelse (random-float 1 < probability_activities)
              [decide_activities_nonreligious_people]
              [move-to home_location]
          ]
      ]
      if (ticks mod 4 = 2 or ticks mod 4 = 3)[
            ifelse (random-float 1 < probability_activities)
              [decide_activities_nonreligious_people]
              [move-to home_location]
     ]

      if ticks mod 4 = 0[
        move-to home_location
      ]
     ]
  ]
  if (day_counter mod 7 = 6)[
     ask people_with_nochild_nojob_noreligion[

       if (ticks mod 4 = 1 or ticks mod 4 = 2 or ticks mod 4 = 3)[
              ifelse (random-float 1 < probability_activities)
                [decide_activities_nonreligious_people]
                [move-to home_location]

       ]

       if ticks mod 4 = 0[
         move-to home_location
         set visits_to_supermarket 0
         set times_walk 0
         set visits_to_religious 0
         set visits_to_initiatives 0
       ]
     ]
  ]
end

to setup-plot
  set-current-plot "Dist Problem Youth"
  set-plot-x-range  0 20
  set-histogram-num-bars 10

  set-current-plot "PLS Histogram"
  set-plot-x-range  0 500
  set-histogram-num-bars 100
end

to do-plot
  set-current-plot "Neighbourhood PLS"
  plot plsglobal

  set-current-plot "Dist Problem Youth"
  set-plot-x-range  0 max list (max [problem-youth] of list_problem_youth_places) 5
  set-plot-y-range  0 count list_problem_youth_places with-max [problem-youth]
  histogram [problem-youth] of list_problem_youth_places; make the histogram

  set-current-plot "PLS Histogram"
  set-plot-x-range floor max(list 0 (min [pls] of people)) ceiling max(list 10 (max [pls] of people))
  set-plot-y-range 0 count people with-max [pls]
  histogram [pls] of people
end

to effect_litter_on_pls
  ask people [
    let count_nearby_litter count (patches in-radius 6 with [litter = True])
    if (count_nearby_litter > 0)[
      set times_litter_seen times_litter_seen + 1

      set pls_decrease_by_litter (pls_initial_decrease_litter * exp (-0.693 * (times_litter_seen - 1)) * ln (1 + 0.3241 * (count_nearby_litter)))

      set pls pls + pls_decrease_by_litter
    ]
    if ((ticks mod 4 = 0) and (day_counter mod 7 = 6))[
      set pls_decrease_by_litter 0
      set times_litter_seen 0
    ]
  ]
end

to effect_interaction_on_pls
;  ask interaction_patches [
  ask people [
    if (member? patch-here interaction_patches)[
       probability_of_interaction_places
    ]
  ]

;]
  ask people [
    if ((ticks mod 4 = 0) and (day_counter mod 7 = 6))[
       set pls_increase_people_interaction 0
       set times_people_interaction 0
    ]
  ]
end

to probability_of_interaction_places
  set pls_first_person pls
  set num_interact 0
  let pls_max_people_sq ((max [pls] of people) ^ 2)
  while [num_interact < 3][
    set times_interaction_temp times_people_interaction
    set num_interact num_interact + 1
    ask one-of people-here [
      set pls_second_person pls
      if ( random-float 1 < ((pls_first_person * pls_second_person)/((pls_max_people_sq)))) [
        set times_people_interaction times_people_interaction + 1
        set pls_increase_people_interaction (pls_initial_increase_people_interaction * exp (-0.231 * (times_people_interaction - 1)))
        set pls pls + pls_increase_people_interaction
      ]
    ]
    if (times_interaction_temp > times_people_interaction)[
       set times_people_interaction times_people_interaction + 1
       set pls_increase_people_interaction (pls_initial_increase_people_interaction * exp (-0.231 * (times_people_interaction - 1)))
       set pls pls + pls_increase_people_interaction
    ]
  ]
end

to probability_of_interaction_walking
  set pls_first_person pls
  set num_interact 0
  let pls_max_people_sq ((max [pls] of people) ^ 2)
  while [num_interact < 3][
    set times_interaction_temp times_people_interaction
    set num_interact num_interact + 1
    ask one-of people with [distance myself <= 50 ]  [
      set pls_second_person pls
      if ( random-float 1 < ((pls_first_person * pls_second_person) / pls_max_people_sq)) [
        set times_people_interaction times_people_interaction + 1
        set pls_increase_people_interaction (pls_initial_increase_people_interaction * exp (-0.231 * (times_people_interaction - 1)))
        set pls pls + pls_increase_people_interaction
        set times_interaction_temp times_interaction_temp + 1
      ]
    ]
    if (times_interaction_temp > times_people_interaction)[
       set times_people_interaction times_people_interaction + 1
       set pls_increase_people_interaction (pls_initial_increase_people_interaction * exp (-0.231 * (times_people_interaction - 1)))
       set pls pls + pls_increase_people_interaction
    ]
  ]
end

to effect_problem_youth_pls
  ask people [
    let nearby_problem_youth list_problem_youth_places_hotspot in-radius 6 with [problem-youth > 0]
    if (any? nearby_problem_youth)[
      set times_problem_youth_seen times_problem_youth_seen + 1
      set times_problem_youth_temp times_problem_youth_seen
      ask one-of nearby_problem_youth [
        set pls_decrease_problem_youth_temp (pls_initial_decrease_problem_youth * (exp (-0.693 * (times_problem_youth_temp))) * (ln (1 + 0.3241 * problem-youth)))
        ; set pls_decrease_problem_youth_temp (pls_initial_decrease_problem_youth * (exp (-0.693 * (1))) * (ln (1 + 0.3241 * problem-youth)))
      ]
      set pls_decrease_by_problem_youth pls_decrease_problem_youth_temp
      set pls pls - pls_decrease_by_problem_youth
    ]

    if ((ticks mod 4 = 0) and (day_counter mod 7 = 6))[
      set pls_decrease_by_problem_youth 0
      set times_problem_youth_seen 0
    ]
  ]

end
@#$#@#$#@
GRAPHICS-WINDOW
233
19
1056
813
-1
-1
1.0
1
10
1
1
1
0
0
0
1
0
814
0
784
1
1
1
ticks
30.0

BUTTON
16
47
89
80
NIL
setup
NIL
1
T
OBSERVER
NIL
S
NIL
NIL
1

BUTTON
17
84
80
117
NIL
go
T
1
T
OBSERVER
NIL
G
NIL
NIL
1

SWITCH
18
169
139
202
verbose?
verbose?
1
1
-1000

SWITCH
18
208
128
241
debug?
debug?
1
1
-1000

SLIDER
16
328
188
361
police-number
police-number
0
10
4.0
1
1
NIL
HORIZONTAL

SLIDER
17
370
189
403
communityworker
communityworker
0
20
2.0
1
1
NIL
HORIZONTAL

MONITOR
1082
53
1153
98
Model Day
day_counter
2
1
11

MONITOR
1082
392
1308
437
Probability Creating Initiatives-Percent
prob-creating-initiative
2
1
11

BUTTON
17
121
98
154
go once
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
17
507
189
540
initial_number_people
initial_number_people
0
3000
300.0
1
1
NIL
HORIZONTAL

TEXTBOX
19
304
169
322
Policies
12
0.0
1

TEXTBOX
18
484
168
502
Meta-Parameters
12
0.0
1

TEXTBOX
1085
20
1235
38
Outputs
12
0.0
1

TEXTBOX
19
20
169
38
Commands
12
0.0
1

MONITOR
1160
53
1239
98
Day of Week
day_counter mod 7 + 1
0
1
11

MONITOR
1081
268
1201
313
Neighbourhood PLS
plsglobal
0
1
11

MONITOR
1082
333
1139
378
Litter
count patches with [litter = true]
17
1
11

MONITOR
1149
333
1224
378
Burglaries
sum [burglarized-times] of people_burglarized
17
1
11

PLOT
1082
112
1282
262
Neighbourhood PLS
Time
Neighbourhood PLS
0.0
300.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot plsglobal"

MONITOR
1319
111
1376
156
Money
money
17
1
11

MONITOR
1234
333
1301
378
Initiatives
count patches with [category = \"neighbourhood initiative\"]
17
1
11

MONITOR
1361
269
1424
314
Max PLS
max [pls] of people
0
1
11

MONITOR
1218
268
1277
313
Min PLS
min [pls] of people
0
1
11

MONITOR
1285
269
1355
314
Mean PLS
mean [pls] of people
0
1
11

MONITOR
1083
457
1229
502
Problem Youth Hotspots
count patches with [problem-youth > 0]
17
1
11

MONITOR
1242
457
1335
502
Problem Youth
sum [problem-youth] of patches
2
1
11

PLOT
1085
526
1285
676
Dist Problem Youth
Number of Youth at Hotspot
Number of Hotspots
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" ""

PLOT
1085
695
1378
927
PLS Histogram
PLS
Number of People
0.0
500.0
0.0
100.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" ""

MONITOR
1299
219
1356
264
Std PLS
standard-deviation [pls] of people
0
1
11

SLIDER
17
412
189
445
garbage-collectors
garbage-collectors
0
10
5.0
1
1
NIL
HORIZONTAL

SWITCH
18
248
121
281
plot?
plot?
0
1
-1000

@#$#@#$#@
## WHAT IS IT?

An agent-based model of Bouwlust, its citizens, police, and community workers; and other properties of the community. The model is intended to offer insight into how policy interventions from the Municipality of The Hague can improve the quality of life in Bouwlust for all its citizens.

## HOW IT WORKS

### Levers

The municipality can influence the neighbourhood's perceived livability and safety (PLS) by adding police officers, community workers, and/or garbage collectors, and by supporting initiatives. Each of these levers cost money, therefore, money was created as a global variable that is changed everytime there is a change in any of those levers. The current distribution of costs is:
  
  - 2 police officers
  - 3 community workers
  - 4 garbage collectors
  - 5 neighbourhood initiatives

We assume that the the current situation does not cost additional funds, so only once we have more than the status quo do we deduct money from the funds. 

### Patches

The patches are imported from a photo of the community that has been edited with a clear red (`pcolor red` – or `15`) boundary. Each patch has the following properties:

  - pcolor: colour displayed on the main interface
  - pcolor-original: the original pcolor; used to reset the patch
  - location: human-friendly name for what the location is useful for
  - category: categorical variable, which is one of the following
    - community centre
    - neighbourhood initiative
    - religious
    - school
    - supermarket
    - police station
  - litter: a boolean indicating if there is litter there or not
  - QR Codes: a boolean indicating if the location has a QR code or not. All locations of specified category have a QR code. 
  - number-visits: an integer that records the number of visits at each neighbourhood initiative. 
  - initiative-viability: a measure of the viability of the initiatives, it increases everytime the initiative is visited. 
  - starting-time: records the starting time of an initiative. Each initiative can only last 6 months 
Neighbourhood initiatives have variables specific to them/ 

### Agents

#### Citizens

The citizens of Bouwlust are one of the main actors, and there are 27,505 of them (adults)! Each of these adults might have these four properties (probability):
 
 - Has children (37%)
 - Has a job (60%)
 - Is religious (50%)
 - Takes part in initiatives (12%)

These percentages are set as variables within the model that can change but use the initial values given above.

The citizens also carry the properties:

 - PLS (see below)
 - Home and work location
 - Nearest special location, which includes schools, initiatives, religious centres, supermarkets.
 - Number of QR codes scanned 
 - Number of initiatives started
 - Number of visits to religous centres
 - Number of visits to initiatives
 - Number of times the person has taken a walk a week
 - Burglarized, which simply counts the time since a burglary (a memory effect)
 - Times burglarized
 - Times police have visited after a burlagry

Employed citizens work five days a week and visit only the important locations nearest to their home.

Citizens visit initiatives randomly, which positively affects their PLS. For now, citizens only visit their nearest initiative by distance. For each initiative, the number of visits are recorded, and their viability increases everytime there is a visit. After six months from its inception, the initiative ends.  

Citizens can create initiatives once they scan a certain number of QR codes (20). If they do not reach that number, the probability of creating initiatives, which is inversely proportional to the global PLS, controls the creation of initiatives. We assume that they can create an initiative at the patch they are located in, as long as there is no exisiting activity there, and the total number of initiatives has not reached 10 yet. Each person has a variable that records how many initiatives started by the person, and creating an intiative has a positive impact on the person's PLS. 

Citizens can also scan QR codes, which have a relatively positive impact on their PLS, though not as much visiting and creating initiatives. 

##### Perceived Livability and Safety (PLS)

The variable that represents perceived liveability and safety is modeled at the level of citizens, as well as at a global neighborhood level. The individual PLS is a metric that starts at 100 at the beginning of the run, and is changed by the following:

  - +20 after police and community worker interactions
  - +10 after visiting initiatives
  - +80 after creating initiatives
  - -1 after seeing litter
  - -80 after a burglary
  - +40 after police visit after a burglary

We assume that the positive view of the neighborhood created by the interactions with residents, police officers, and engagement in initiatives slowly degrades over time. Thus, at the end of every day, the individual pls reduces by 2%.  

In order to have a representative global PLS, it is calculated as the average of the individuals PLS times the standard deviation: `global pls = mean(pls) * std(pls)/mean(pls)`.

It is important to note that the PLS values in this model are abstract and not rooted in some physical entity. They are kept as integers for simplicity with 1, the lowest value, set for seeing litter. Each value above that should be viewed proportionally to one another. These proportions are mostly based on Figure 6 of Bonaiuto, M. *et al.* (1999). The relevance of this index relative to others and to policy use is discussed in Bonaiuto *et al.* (2015).

#### Police Officers 

Police Officers are a specific breed of turtles. We assume the following:

  - Officers start their day at the police station, they remain in the station until sent out.
  - When there is a report of a problem youth/litter hotspot, a number of police officers are sent to that location.
  - While they are walking to that location, they can interact with citizens.
  - At the end of their shift, they return to the police station. 

A police officer goes to a random location (patch) with litter, problem youth, or someone who has been burglarized and not yet visited by the police. Once there, the officer deals with the litter by removing it, cause the youth to flee to another location, or console the burglarized person (increasing their PLS). We chose to send officers to random locations in order to avoid two police officers going to the same location, as that would be a waste of resources. 

Police officers follow a daily schedule, as do all citizens and community workers. The day is divided into 4 ticks. The first tick signals the beginning of the day, so the police officers begin moving towards problematic areas. The police will prioritize visiting people who have been burglarized but only 20% of the time to represent underreporting (based on some [Dutch national statistics](https://www.dutchnews.nl/news/2018/05/dutch-reported-crime-rate-continues-to-fall-new-cbs-report-shows/)). Otherwise, police favour visiting problem youth 90% of the time over litter. We assume they move a randomized proportion of the distance there. Then at the second tick, they continue their way to the problematic area. At the third tick, the police officer begin returning to the station (moving a randomized proportion of the distance). At the fourth and final tick, they are at the station, where they finish their day. At every tick, the police officers interact with the citizens in a radius of 10. 

#### Community Workers

Community workers start their day at the community center, and visit the nearest neighbourhood initiative.To avoid the community workers visiting the same initiative, we limited the set of possible targets as neighborhood initiatives where there is no community worker at the time. Every visit increases the viability of the initiative. 

Community workers  follow a daily schedule, similar to the one defined for police officer workers. The day is divided into 4 ticks. The first tick signals the beginning of the day, so the commuity workers begin moving towards initiatives. Similarly to police officers, we assume they move halfway there. They continue their way at the second tick, and at the third tick, they begin returning to the centre (moving half of the distance). At the fourth and final tick, they are back at the cenntre, where they finish their day. Similarly to police officers, at every tick,  the community workers interact with the citizens in a radius of 4, but with a different impact on the PLS.

#### Agent Schedules

Agents can undertake activities in any of four ticks. This model does not model nighttime since nothing happens then. For example, burglaries are assigned at the beginning of each new day (i.e. they are assumed to occur at night), though many burglaries actually happen during the day. But, for the purpose of the model, this semantic difference is not important to distinguish.

Agents' schedules are based on the variable `day_counter`, which begins at 0 and is updated once every four ticks, which translates to once per day at the beginning of the day. Weekends are defined as when `day_counter mod` is 5 or 6.


### Neighbourhood Attributes

#### Problem Youth

Problem youth are delinquent youth that loiter around schools and malls (supermarkets). They are considered to have a negative impact on the livability of the neighborhood. In this model, a certain number of young people emerges at certain patches at a frequency inverserly proportional to the overall PLS. 

Problem youth spawn every tick (as an integer property of patches) and favour places where there are already problem youth with a 85% bias. They spawn at a rate inversely proportional to PLS with 5 per tick when PLS is 0 and 0 at the PLS pls_upper_threshold of 20000. If there are more than 20 problem youth in one spot, 25, 50, or 75% of the group will leave for another spot where there are no problem youth already.

They also create litter around where they are relative to how many of them are gathered. When there are 20 of them (the "soft" maximum), then half of the patches in a 3-patch radius will theoretically be littered. However, the function currently might ask patches that already have litter on it to be littered again, which is not exactly realistic, but sufficient for this model.

Problem youth also affects the individual PLS of citizens. The PLS decreases depending on the number of problem youth preseant at the location and the number of times the citizen sees problem youth in a given week. The impact of number of problem youth at the location on PLS is modelled by a logarthmic relation and the impact of number of times a citizen sees problem youth in a week is modelled by an exponentially decreasing function with coefficient -0.693 '(ln 2)' (implies that if a citizen sees problem youth second time in a week the decrease in PLS will be half of what it was the first time he/she saw problem youth)

Due to their disruptive nature and negative impact on individual PLS, problem youth are dealt with by police officers who go to their locations. Once they see the police, the youth move to another location, with a percentage of them (20%) quitting the group and going home in fear of the police. The percentage that changes is rounded down (e.g. if 5 flee the police, 4 will remain, but if 2 flee, 1 remains, and if 1 flees, then 0 remain).


#### Litter

Litter is created by problem youth when they are gathered and randomly too by all citizens. For the latter, this model simply creates them randomly around the neighbourhood at a rate proportional to overall PLS (`plsglobal`).

We assumed that this relationship between PLS and litter is inversely proportional where 0 PLS means 10% of the neighbourhood will be littered and once a pls_upper_threshold PLS value of 100 has been reached, 0.5% of the neighbourhood will be littered. 

Furthermore, the impact of litter on individual pls of citizens is modelled as follows:

1) In a particular week, the number of times an agent sees a litter is calculated. We assume that an agent will see litter if he/she is in a radius of 20m of litter. 

2) For the first time an agent sees litter in a week, individual PLS decreases by 5

3) for next times, he/she sees litter, the pls decreases but the decrease in pls itself decreases exponentially with the number of times he/she sees litter. The exponential coefficient is -0.693 '(-ln 2)'. So, if an agent sees litter second time in a week, pls decreases by 2.5 and so on.


### Interaction between citizens
Interaction between citizens has an effect on the PLS. However, the probability of interaction itself depends on the PLS of the pair of citizens being considered. Citizens can interact with other citizens in places like supermarket,school, community centre, neighbourhood initiative,religious place and while walking. The probability of interaction is modelled as the product of percentage pls of pair of citizens being considered relative to the square of maximum pls among all the citizens. The number of times citizen interacts with other citizens in a week is calculated. For the first interaction of citizen with fellow citizen in a week, pls increases by 5. for next interactions, the PLS increases but the increase in PLS itself decreases exponentially with number of interactions with exponential coefficient -0.231 '(ln 2)/3'. so for the fourth interaction, pls increases by 2.5 and so on. At any interaction place, citizens can interact with a maximum of 4 other citizens and not more.

#### QR Codes

Nearly every location that is not a specific location (supermarket, school, initiative, community center, police station) contains a QR code that can be scanned by citizens during their walk. This has a small effect on the individual PLS, as it changes it by one of three values, selected randomly: 0, +8, +20.  

We assume that citizens can only scan QR codes when they are on a leisurly walk, as they are then looking around and willing to take the time to learn more about the neighborhood. 

#### Neighborhood Initiatives

The model begins with 5 existing initiatives, which are visited by both community workers and citizens. These initiatives last for 6 months (720 ticks in the model), and have a viability variable that increases with every visit (+1 after citizens visit, +3 after community workers visit). 

To account for the effect of memory over the perception of initiatives, seeing as engaging in initiatives becomes less attractive over time, a degradation function has been modelled. At the end of every week since the beginning of the initiative, its viability reduces by 25%. 

Citizens can create initiatives, with a maximum of 10 initiatives existing at the same time, but it depends on the following factors:

  - Probability of Creating Initiatives: there is a probability that is inversely proportional to the global PLS that controls the number of initiatives created. It is assumed that the higher the PLS, the less citizens would want to get involved in improving their community, as there is a perception that things are going well. Inversely, once the PLS reaches a certain low pls_upper_threshold, more citizens are eager to create initiatives.
  - QR codes Scanned: scanning QR codes expresses interest and committment to the nieghborhood, so once a citizen scans a certain number of QR codes (20), they would want to create an initiative. 

It is assumed that citizens can only create initiatives when they are on a leisurly walk.

#### Burglaries

Burglaries 

According to [knoema.com](https://knoema.com/atlas/Netherlands/topics/Crime-Statistics/Burglary-Car-Theft-and-Housebreaking/Burglary-rate), the 2016 burglary rate for all of The Netherlands was 0.5%, though it has also been dropping over time – it was 2% in 2006. As stated in the police section, not every citizen reports burglaries! This model uses a 20% reporting rate (based on [Dutch national statistics](https://www.dutchnews.nl/news/2018/05/dutch-reported-crime-rate-continues-to-fall-new-cbs-report-shows/)), which is implemented as how likely police are to actually visit a victim. Therefore, the true burglary rate needs to be represented as `0.5% / 20%`, which is 2.5%. Using this same logic and reporting rate, the worst burglary rate in the world in 2016 was Chile at 5.5% and the best in high GDP countries (both according to knoema.com) was Singapore at 0.019%. Therefore, to account for even more anarchy, the rate at 0 PLS will be set to 10% to represent a worst case. However, this is an annual value and this model's agents represent more than one actual person. Therefore, the worst case daily probability of being burglarized is `(1 - (1 - 10%) ^ (1 / 365)) * #agents / 27505`.

### Time

Each day in this model is divided into four ticks: morning, mid-day, afternoon, and evening. The model was built to run for an experiment period of three years, though it can technically long indefinitely (as long as the observer wants).

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

Clone this project from https://github.com/jasonrwang/bouwlust-abm.

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

- Bonaiuto, M., Aiello, A., Perugini, M., Bonnes, M., & Ercolani, A. P. (1999). Multidimensional Perception of Residential Environment. Journal of Environmental Psychology, 19, 331–352.
- Bonaiuto, M., Fornara, F., Ariccio, S., Ganucci Cancellieri, U., & Rahimi, L. (2015). Perceived residential environment quality indicators (PREQIs) relevance for UN-HABITAT city prosperity index (CPI). Habitat International, 45(P1), 53–63. https://doi.org/10.1016/j.habitatint.2014.06.015

This project is hosted online at https://gitlab.com/jasonrwang/bouwlust-abm.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.1.1
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="experiment" repetitions="3" sequentialRunOrder="false" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="60"/>
    <metric>money</metric>
    <metric>plsglobal</metric>
    <enumeratedValueSet variable="police-number">
      <value value="2"/>
      <value value="19"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial_number_people">
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="debug?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="communityworker">
      <value value="5"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="verbose?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
